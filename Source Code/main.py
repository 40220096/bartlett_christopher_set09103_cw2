from __future__ import division
from flask import Flask, url_for, request, render_template, jsonify, session, flash, redirect
import json, os
from sqlalchemy import *
from sqlalchemy import exc
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Email, Length
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.datastructures import MultiDict
from flask_socketio import SocketIO, send, emit, join_room, leave_room
from datetime import datetime


app = Flask(__name__)
Bootstrap(app)

app.secret_key = 'A0Zr98j/3yXR~XfHH!jmN]LWX/,?RT'
socketio = SocketIO(app)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////'+os.path.join(BASE_DIR, "database.db")

db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

users = {}
##db.create_all()
queue = []

matchList = []

##CREATE TABLE User (id integer PRIMARY KEY, username text NOT NULL UNIQUE, email text NOT NULL UNIQUE, password text NOT NULL);

class MatchData():
	#match_id = id
	
	def __init__(self, id, firstUser, secondUser):
		self.match_id = id
		self.yellow = firstUser
		self.red = secondUser
		self.turn = firstUser
		self.board = [
			[0,0,0,0,0,0],
			[0,0,0,0,0,0],
			[0,0,0,0,0,0],
			[0,0,0,0,0,0],
			[0,0,0,0,0,0],
			[0,0,0,0,0,0],
			[0,0,0,0,0,0]
			]
		self.winner = None
		self.turn_counter = 0


class User(UserMixin, db.Model):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(15), unique=True)
	email = db.Column(db.String(40), unique=True)
	password = db.Column(db.String(80))
	
class Match(db.Model):
	match_id = db.Column(db.Integer, primary_key=True)
	player1_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	player2_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
	winner_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
	turns = db.Column(db.Integer, nullable=True)
	is_active = db.Column(db.Boolean, nullable=False)
	date = db.Column(db.DateTime, nullable=False)
	
@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

class LoginForm(FlaskForm):
	username = StringField('username', validators=[InputRequired(), Length(min=4, max=14)])
	password = PasswordField('password', validators=[InputRequired(), Length(min=4, max=80)])
	remember = BooleanField('remember me')

class RegisterForm(FlaskForm):
	email = StringField('email', validators=[InputRequired(), Length(min=4, max=40), Email(message='Invalid Email!')])
	username = StringField('username', validators=[InputRequired(), Length(min=4, max=14)])
	password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])
	
class EditProfileForm(FlaskForm):
	email = StringField('new_email', validators=[InputRequired(), Length(min=4, max=40), Email(message='Invalid Email!')])
	username = StringField('new_username', validators=[InputRequired(), Length(min=4, max=14)])
	

@socketio.on('message')
@login_required
def connect(msg):
	print(request.sid)
	##print (str(current_user.id) +': ' + msg)
	send(current_user.username +': '+msg, broadcast=True)
	if current_user.username not in users:
		users[current_user.username] = request.sid	
	elif users[current_user.username] != request.sid:
		users[current_user.username] = request.sid
	print users

@socketio.on('connect')
@login_required
def test_connect():
    print 'just established connection:' + current_user.username + '/' + request.sid;
    update_sid(current_user, request)
    print '#########'
    print users
    return

def update_sid(user, request):
	if user.username not in users:
		users[user.username] = request.sid	
	elif users[user.username] != request.sid:
		users[user.username] = request.sid
		
	return
	
	
@socketio.on('match_response')
@login_required
def match_response(data):

	update_sid(current_user, request)	
	print data
	matchdata = None
	
	if data['match_id'] is None:
		return
	
	for match in matchList:
		if str(match.match_id) == data['match_id']:
			matchdata = match
			continue
	
	
	if matchdata is None:
		print '### matchdata is None in match_response'
		return
	
	##Check if its current user's turn
	
	print matchdata.turn 
	print current_user.id
	
	
	if matchdata.turn != current_user.id:
		print 'wrong turn pal'
		if 'surrender' not in data: 
			return

	match = Match.query.filter_by(match_id=matchdata.match_id).first()
	
	
	if match is None:
		print '### match is None in match_response'
		return
	
	if match.is_active:
		if match.player1_id == current_user.id or match.player2_id == current_user.id:
			user1 = User.query.filter_by(id=match.player1_id).first()
			user2 = User.query.filter_by(id=match.player2_id).first()
			
			matchboard = matchdata.board
			
			
			if 'surrender' in data:
				tmpwinner = None
				if current_user.id == user1.id:
					tmpwinner = user2.id
				elif current_user.id == user2.id:
					tmpwinner = user1.id
					
				if tmpwinner is not None:
					#end the game as someone has surrendured.
					payload = {}
					payload['winnerBySurrender'] = tmpwinner
					
					match.winner_id = tmpwinner
					match.turns = matchdata.turn_counter
					match.is_active = False
					db.session.commit()
					
					emit('match_response', payload, room=users[user1.username])
					emit('match_response', payload, room=users[user2.username])
					return
				
				
				
			
			if data['turn'] is None:
				print 'turn was none'
				return
			
			if data['turn'] == 'surrender':
				##user has surrendered, close match in db and emit match finished payload
				print ''
					
			tmpt = data['turn'].replace("x-", "")
			
			tmpturn = int(tmpt) -1
			print tmpturn 
			print 'wah4'
			##This row is entirely filled up
			if matchboard[tmpturn][5] != 0:
				##emit stuff that tells the guys its the same persons turn and their turn was invalid
				print 'invalid, row is already filled to top'
				return
			
			for index in range(0, len(matchboard[tmpturn])):
				if matchboard[tmpturn][index] == 0:
					##print str(tmpturn) +', '+str(index)+' = ' +str(matchboard[tmpturn][index])+' was 0'
					##print 'aaaaaanndd ' + str(matchdata.turn) + 'vs current user id: ' + str(current_user.id) 
					if matchdata.turn == current_user.id and matchdata.yellow == current_user.id:
						matchboard[tmpturn][index] = 1
						matchdata.turn = int(matchdata.red)
						matchdata.turn_counter += 1
						break
					elif matchdata.turn == current_user.id and matchdata.red == current_user.id:
						matchboard[tmpturn][index] = 2
						matchdata.turn = int(matchdata.yellow)
						if matchdata.turn_counter == 21:
							matchdata.turn_counter += 1
						break
				else:
					print str(tmpturn) +', '+str(index)+' = ' +str(matchboard[tmpturn][index])+' was not 0'
			
			print 'have now processed users turn'
			print matchboard
			
			
			
			
			payload = {'turn_user': matchdata.turn,
						'turn': matchdata.turn_counter,
						'boarddata': matchboard
						}
			##checkForWinner(matchdata.yellow, matchboard)
			##print matchdata.b
			
			winner1 = checkForWinner(1, matchboard, payload)
			winner2 = checkForWinner(2, matchboard, payload)
			
			if winner1:
				payload['winner'] = user1.id
				match.winner_id = user1.id
			elif winner2:
				payload['winner'] = user2.id
				match.winner_id = user2.id
			elif matchdata.turn_counter > 21:
				payload['winner'] = 1
				match.winner_id = 1
				payload['turn'] = payload['turn'] -1
			
			if winner1 or winner2 or matchdata.turn_counter > 21:
				match.turns = matchdata.turn_counter
				match.is_active = False
				db.session.commit()
			
			emit('match_response', payload, room=users[user1.username])
			emit('match_response', payload, room=users[user2.username])	##, room=matchdata.match_id
		
		
def checkForWinner(player, matchboard, payload):
	
	#print 'vertical' 
	for h in range(len(matchboard[0]) -3):
		#print(h, matchboard[0][h+3])
		for w in range(len(matchboard)):
			#print(w, matchboard[w][h], matchboard[w][h+1], matchboard[w][h+2], matchboard[w][h+3])
			if matchboard[w][h] == player and matchboard[w][h+1] == player and matchboard[w][h+2] == player and matchboard[w][h+3] == player:
				#print '^ found ^'
				payload['winning_tiles'] = [
					'x'+str(w+1)+'y'+str(h+1),
					'x'+str(w+1)+'y'+str(h+2),
					'x'+str(w+1)+'y'+str(h+3),
					'x'+str(w+1)+'y'+str(h+4)
				]
				return True
	
	#print 'horizontal'
	for w in range(len(matchboard) -3):
		#print(h, matchboard[0][h+3])
		for h in range(len(matchboard[0])):
			#print(h, matchboard[w][h], matchboard[w+1][h], matchboard[w+2][h], matchboard[w+3][h])
			if matchboard[w][h] == player and matchboard[w+1][h] == player and matchboard[w+2][h] == player and matchboard[w+3][h]  == player:
				#print '^ found ^'
				payload['winning_tiles'] = ['x'+str(w+1)+'y'+str(h+1),'x'+str(w+2)+'y'+str(h+1),'x'+str(w+3)+'y'+str(h+1), 'x'+str(w+4)+'y'+str(h+1)
				]
				return True
		
	#print 'ascending diagonal'
	for w in range(3, len(matchboard)):
		#print(h, matchboard[0][h+3])
		for h in range(len(matchboard[0]) -3 ):
			#print(h, matchboard[w][h], matchboard[w-1][h+1], matchboard[w-2][h+2], matchboard[w-3][h+3])
			if matchboard[w][h] == player and matchboard[w-1][h+1] == player and matchboard[w-2][h+2] == player and matchboard[w-3][h+3] == player:
				#print '^ found ^'
				payload['winning_tiles'] = [
					'x'+str(w+1)+'y'+str(h+1),
					'x'+str(w)+'y'+str(h+2),
					'x'+str(w-1)+'y'+str(h+3),
					'x'+str(w+-2)+'y'+str(h+4)
				]
				return True
			
			
	#print 'descending diagonal'
	for w in range(3, len(matchboard)):
		#print(h, matchboard[0][h+3])
		for h in range(3, len(matchboard[0])):
			#print(h, matchboard[w][h], matchboard[w-1][h-1], matchboard[w-2][h-2], matchboard[w-3][h-3])
			if matchboard[w][h] == player and matchboard[w-1][h-1] == player and matchboard[w-2][h-2] == player and matchboard[w-3][h-3] == player:
				#print '^ found ^'
				payload['winning_tiles'] = [
					'x'+str(w+1)+'y'+str(h+1),
					'x'+str(w)+'y'+str(h),
					'x'+str(w-1)+'y'+str(h-1),
					'x'+str(w-2)+'y'+str(h-2)
				]
				return True
	
	return False
	
@socketio.on('queue_match')
@login_required
def queue_match(msg):
	print(request.sid)
	send(current_user.username +': '+msg, broadcast=True)
		
	update_sid(current_user, request)
		
	##print users
	
	
	if len(queue) > 0:
		print 'queue > 0'
		## Match found, because someone already in queue
		user1 = User.query.filter_by(username=queue[0]).first()
		user2 = User.query.filter_by(username=current_user.username).first()
		
		##Stop user queuing against themselves
		if user1.id == user2.id:
			return

		new_match = Match(player1_id=user1.id, player2_id=user2.id, is_active=True, date=datetime.now())
		db.session.add(new_match)
		db.session.commit()

		join_room(room=new_match.match_id, sid=users[user1.username])
		join_room(room=new_match.match_id, sid=users[user2.username])
		
		new_matchdata = MatchData(new_match.match_id, user1.id, user2.id)
		
		matchList.append(new_matchdata)

		emit('queue_match_response', {'match_id':new_match.match_id}, room=new_match.match_id)
		queue.remove(user1.username)
		
	else:
		queue.append(current_user.username)
		print 'queue not > 0'
		print queue
	

@app.route('/')
@login_required
def home():
	return render_template('home.html', user=current_user), 200
	
@app.route('/match')
@app.route('/match/')
@app.route('/match/<int:match_id>')
@login_required
def get_match(match_id = None):
	print match_id
	if match_id is None:
		return redirect(url_for('home'))
	
	match = Match.query.filter_by(match_id=match_id).first()
	
	if match is None:
		return render_template('pre_match.html', errorMessage='No match found under ID '+str(match_id), user=current_user)
		
	user1 = User.query.filter_by(id=match.player1_id).first()
	user2 = User.query.filter_by(id=match.player2_id).first()
		
	return render_template('match.html', match=match, user=current_user, user1=user1, user2=user2)
		
	
@app.route('/leaderboards/<type>')
def get_leaderboards(type = None):
	if type is None:
		return redirect(url_for('home'))
		
	if type == 'winners' or type == 'losers':
		return render_template('leaderboard.html', user=current_user, type=type)

	else:
		return redirect(url_for('home'))
	
@app.route('/api/stats/leaderboards/getWinners', methods=['GET'])
def api_leaderboards_winners():
	payload = {}
	payload['data'] = []
	
	result = db.engine.execute('SELECT *, (SELECT count(*) from Match where winner_id = id) as wins, (SELECT count(*) from Match where winner_id is not null AND winner_id != id AND (player1_id = id OR player2_id = id)) as losses from User where wins > 0 ORDER BY wins DESC LIMIT 10')
	
	if result is None:
		return jsonify('could not complete request')
	
	for row in result:
		tmp = {}
		tmp['id'] = row['id']
		tmp['username'] = row['username']
		tmp['wins'] = row['wins']
		tmp['losses'] = row['losses']
		tmp['total_games'] = tmp['wins'] + tmp['losses']
		tmp['winrate'] = '%.2f' % ((tmp['wins'] /  tmp['total_games']) * 100)
		payload['data'].append(tmp)
		
	return jsonify(payload)
	
@app.route('/api/stats/leaderboards/getLosers', methods=['GET'])
def api_leaderboards_losers():
	payload = {}
	payload['data'] = []
	
	result = db.engine.execute('SELECT *, (SELECT count(*) from Match where winner_id = id) as wins, (SELECT count(*) from Match where winner_id is not null AND winner_id != id AND (player1_id = id OR player2_id = id)) as losses from User where wins > 0 ORDER BY losses DESC LIMIT 10')
	
	if result is None:
		return jsonify('could not complete request')
	
	for row in result:
		tmp = {}
		tmp['id'] = row['id']
		tmp['username'] = row['username']
		tmp['wins'] = row['wins']
		tmp['losses'] = row['losses']
		tmp['total_games'] = tmp['wins'] + tmp['losses']
		tmp['winrate'] = '%.2f' % ((tmp['wins'] /  tmp['total_games']) * 100)
		payload['data'].append(tmp)
		
	return jsonify(payload)	

	
@app.route('/api/stats/matchstats/', methods=['POST'])
def api_matchstats():
	print request.form
	
	data = request.form
	
	if 'user1' in data and 'user2' in data:
		result = db.engine.execute('SELECT * from Match where (player1_id ='+data['user1']+' AND player2_id ='+data['user2']+') OR (player1_id ='+data['user2']+' AND player2_id ='+data['user1']+') AND winner_id NOT NULL')
		payload = {}
		payload['total_games'] = 0
		payload['wins'] = 0 
		payload['losses'] = 0
		payload['draws'] = 0

		tmpuser = User.query.filter_by(id=data['user2']).first()
		
		if tmpuser is None:
			return jsonify('could not complete request')
		
		payload['opponent'] = tmpuser.username
		
		if result is not None:
			for row in result:
				print row['winner_id']
				payload['total_games']+=1
				if row['winner_id'] == int(data['user1']):
					payload['wins']+= 1
				elif row['winner_id'] == int(data['user2']):
					payload['losses']+=1
				else:
					payload['draws']+=1
			
			payload['winrate'] = '%.2f' % ((payload['wins'] /  payload['total_games']) * 100)
			return jsonify(payload)
			
		else:
			return jsonify('could not complete request')

	return jsonify('could not complete request')
	
@app.route('/api/user/getMatchData/<int:id>', methods=['GET'])
def get_match_data(id = None):
	if id is None:
		print 'id is none in getMatchData'
		return jsonify('could not complete request')
	
	user = User.query.filter_by(id=id).first()
	if user is None:
		print 'user is none for ID: '+id
		return jsonify('could not complete request')
		
	payload = []
	
	result = db.engine.execute('SELECT * from Match where (player1_id = '+str(user.id)+' OR player2_id = '+str(user.id)+') AND winner_id IS NOT NULL')
	
	if result is None:
		print 'result of db query is none'
		return jsonify('could not complete request')
		
	for row in result:
		
		tmp = {}
		tmp['match_id'] = row['match_id']
		
		user2 = None
		
		if row['player1_id'] == id:
			user2 = User.query.filter_by(id=row['player2_id']).first()
		else:
			user2 = User.query.filter_by(id=row['player1_id']).first()
			
		if user2 is None:
			print 'user2 is none in getMatchData'
			return jsonify('could not complete request')
			
		if row['player1_id'] == id:
			tmp['player1'] = user.username
			tmp['player2'] = user2.username
		else:
			tmp['player2'] = user.username
			tmp['player1'] = user2.username
		
		if row['winner_id'] == id:
			tmp['winner'] = user.username
		elif row['winner_id'] == user2.id:
			tmp['winner'] = user2.username
		elif row['winner_id'] == 1:
			tmp['winner'] = 'Draw'
		else:
			tmp['winner'] = 'Incomplete'
			
		tmp['turns'] = row['turns']
		tmp['match_date'] = row['date']
		
		payload.append(tmp)
		
	res = {}
	res['data'] = payload
	return jsonify(res)


@app.route('/profile/edit', methods=['POST'])
def edit_profile():
	user = current_user
	username = request.form.get("new_username")
	email = request.form.get("new_email")
	'''data = MultiDict(mapping=request.json)
	form = EditProfileForm(data)
	if form.validate():
		print 'noice'
	else:
		print 'woops'''
	print username, email
	
	payload = {}
	
	if username is not None and len(username) >= 4 and len(username) <= 14 and user.username != username:
		user.username = username
		try:
			db.session.commit()
			payload['username'] = 'success'
		except exc.SQLAlchemyError:
			payload['username'] = 'error'
	elif len(username) == 0:
		payload['username'] = 'didnotsubmit'	
	else:
		payload['username'] = 'error'
		
	if email is not None and len(email) >= 4 and len(email) <= 40 and user.email != email:		
		user.email = email
		try:
			db.session.commit()
			payload['email'] = 'success'
		except exc.SQLAlchemyError:
			payload['email'] = 'error'		
	elif len(email) == 0:
		payload['email'] = 'didnotsubmit'	
	else:
		payload['email'] = 'error'	
		
	return jsonify(payload)
	
	
@app.route('/profile/<username>')
@app.route('/profile/<int:id>')
@app.route('/profile')
@app.route('/profile/')
@login_required			
def get_profile(username = None, id = None):

	user = None
	
	if username is not None:
		user = User.query.filter_by(username=username).first()
		
	elif id is None:
		id = current_user.id
	
	if id is not None:
		user = User.query.filter_by(id=id).first()
				
	if user is not None:	
		match_count = db.engine.execute('SELECT count(*) as count from Match where (player1_id = '+str(user.id)+' OR player2_id = '+str(user.id)+') AND winner_id IS NOT NULL')
		match_wins = db.engine.execute('SELECT count(*) as wins from Match where (player1_id = '+str(user.id)+' OR player2_id = '+str(user.id)+') AND winner_id = '+str(user.id))
		
		##print match_count.fetchone()[0]
		##print match_wins.fetchone()[0]
		data = {}
		for row in match_count:
			data['match_count'] = int(row['count'])
			
		for row in match_wins:
			data['match_wins'] = int(row['wins'])

		data['winrate'] =  '%.2f' % (( data['match_wins'] / data['match_count'] ) * 100)

		return render_template('profile.html', userprofile=user, data=data, user=current_user)
	
	return redirect(url_for('home'))

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect(url_for('login'))
	
@app.route('/login', methods=['GET','POST'])
def login():
	form = LoginForm()
	
	if form.validate_on_submit():
		user = User.query.filter_by(username=form.username.data).first()
		if user:
			if check_password_hash(user.password, form.password.data):
				login_user(user, remember=form.remember.data)
				return redirect(url_for('home'))
		return 'Invalid username or password'
	return render_template('login.html', form=form)
	

@app.route('/register', methods=['GET','POST'])
def register_account():
	form = RegisterForm()
	if form.validate_on_submit():
		hashed_pw = generate_password_hash(form.password.data, method='sha256')
		new_user = User(username=form.username.data, email=form.email.data, password=hashed_pw)
		db.session.add(new_user)
		db.session.commit()
		login_user(new_user, remember=True)
		return redirect(url_for('home'))
	return render_template('register.html', form=form)
		
				
	
@app.errorhandler(404)
def page_not_found(error):
	if not session.get('logged_in'):
		return redirect(url_for('login'));
	return render_template('error.html', user=session['username']);


if __name__ == "__main__":
	##app.run(host='0.0.0.0' ,debug=True, use_reloader=false)
	socketio.run(app)
